<?php

require_once('config.php');
require_once('PHPMailer/class.phpmailer.php');

//connect to database

$connection = mysql_connect('localhost',username,password);
mysql_select_db(database ,$connection) or die(mysql_error());

//Get the info we need

$query="SELECT * FROM candidates";

$result=mysql_query($query) or die(mysql_error());

//And close connection

mysql_close();

//Count the number of rows collected a.k.a number of candidates

$numberOfCandidates = mysql_numrows($result);

//Put candidates names into an array

while($row = mysql_fetch_array($result)){
	$candidates[] = $row['FirstName'];
}

//See what day it is

$jd = cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
$day = jddayofweek($jd,1);

//Define where this weeks past teamasters list is

$WeekNames = dirname(__FILE__) . "/yesterday.txt";

//If Monday reset the list

if($day == "Monday"){
	$FileHandleReset = fopen($WeekNames, 'w') or die("can't open file (Monday)");
	$reset = fwrite($FileHandleReset, "");
}

//Get past teamasters list and enter names into an array

$WeekNamesString = file_get_contents($WeekNames, true);
$names = explode(",", $WeekNamesString,-1);

//Generate a numer from 0 to (number of candidates)

function randomNumber($i){
	$random = rand(0, $i -1);
	return $random;
}

//Count the number of this weeks names to all candidates. 
//If they are the same that meant there arnt any more candidates available

$a = count($names);
$b = count($candidates);
$noMoreCandidates = false;

if($a == $b){
	$noMoreCandidates = true;
}
function who($names,$numberOfCandidates,$WeekNames,$noMoreCandidates){
	global $candidates;
	
	//choose a name from the candidate array using a random number
	
	$TodaysName = $candidates[randomNumber($numberOfCandidates)];
	
	if($noMoreCandidates){
		exit("Cant select as everybody has been teamaster this week");
	}
	//Re-choose if todays name is already in the list of this weeks past list
	
	else if (in_array($TodaysName, $names)){
	   who($names,$numberOfCandidates,$WeekNames,$noMoreCandidates);
   }
   //Else add todays TEAmaster to this weeks list and send Email
   
	else{
		$FileHandle = fopen($WeekNames, 'a') or die("can't open file");
		$write = fwrite($FileHandle, $TodaysName . ",");
		fclose($FileHandle);
		
		echo "Todays teamaster is " . $TodaysName;
	
		send_mail($TodaysName,sendTo,sentFrom);	
	}
}
//Run choosing function

who($names,$numberOfCandidates,$WeekNames,$noMoreCandidates);

function send_mail($teamaster,$sendTo,$sentFrom){
	
	global $day;
	
	$htmlBody = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
	<html>
	<head>
		<meta charset='utf-8'>
		<title>TEAmaster</title>
	</head>
	<body>
		<table width='600' border='0' cellpadding='0' cellborder='0' align='center'>
			<tbody>
				<tr>
					<td>
						<img src='http://teamaster.other.co.uk/images/email.jpg' alt='TEAmaster'>
					</td>
				</tr>
				<tr>
					<td>
						<p style='font-family:Helvetica Neue,Helvetica;font-size:39px;text-align:center;color:grey;'>". $day . "'s <span style='font-weight:bold;'>TEA</span>master is " . $teamaster ."</p>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
	</html>";
	
	$mail = new PHPMailer(true);
	$mail->AddAddress($sendTo);
	$mail->SetFrom($sentFrom, 'TEAmaster');
	$mail->Subject = $day . 's TEAmaster is ' . $teamaster;
	$mail->Body = $htmlBody;
	$mail->isHTML(true);
	
	if(($day == "Saturday") || ($day == "Sunday")){
		//its the weekend, no need to send email out
	}
	else{
		//Send out email informing people who is the TEAmaster today
		$mail->send(); 
	}
	
}

?>