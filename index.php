<?php

$listOfNames = file_get_contents('yesterday.txt', true);
$namesArray = explode(",", $listOfNames,-1);

$teamaster = end($namesArray);

$jd=cal_to_jd(CAL_GREGORIAN,date("m"),date("d"),date("Y"));
$day = jddayofweek($jd,1);

?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TEAmaster</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link rel="image_src" href="facebook-icon.png" title="facebook-icon"/>
	<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png" />
	
	<link href="stylesheets/app.css" rel="stylesheet" type="text/css">
	<script src="javascripts/foundation/modernizr.foundation.js" type="text/javascript"></script>
	
	<style>
	p{
		font-family: inherit;
		font-weight: normal;
		font-size: 39px;
		line-height: 1.6;
		margin-bottom: 27px;
		color: grey;
		text-align: center;
	}
	span{
		font-weight: bold;
	}
	</style>
	
</head>
<body>
	
	<div class="row">
		<div class="nine columns centered">
			<img src="images/teamaster.jpg" />
		</div>
	</div>
	<div class="row">
		<div class="nine columns centered">
			<p><?php echo $day ?>'s <span>TEA</span>master is <?php echo $teamaster ?></p>
		</div>
	</div>
	
	
</body>
</html>

