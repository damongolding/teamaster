$(document).ready(function(){
	var foo,
		remove = true,
		add = true;
	$(document).on("click","#add",function(){
		$("#addName").reveal();
		$("input").focus();
	})
	.on("click","#remove",function(){
		if(remove){
			$(this).closest("li").addClass("active");
			$("i").css("opacity","1");
			$("#main li").css("margin-left","0px");
			remove = false;
		}
		else{
			$(this).closest("li").removeClass("active");
			$("i").css("opacity","0");
			$("#main li").css("margin-left","-15px");
			remove = true;
		}
	})
	.on("click",".add_btn",function(){
		foo = $("input").val();
		var	b = "insert.php?n=" + foo;
		$.post( b , function(data) {
		  if(data != null){
			  $("#addName").trigger('reveal:close');
			  $("#main li").last().after("<li id=" + foo + "><i class='foundicon-minus'></i>" + foo + "</li>");
		  }
		  else{
			  $("input").addClass("error").val("Something went wrong!");
		  }
		});
	});
	
	$("i").on("click",function(){
		var a = $(this).closest("li").text(),
			b = "delete.php?n=" + a,
			c = $(this).parent();
		$.post( b , function(data) {
			  c.remove();
			  $("i").css("opacity","1");
		});
	});
});